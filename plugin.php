<?php
/*
Plugin Name: The Events Calendar - Shortcodes
Plugin URI: https://www.boldorion.com
Description: Helpful shortcodes to make theming easier.
Version: 1.0.2
Author: Duncan Platt
Author URI: https://www.boldorion.com
License: Proprietary
*/

/*
 * Let's ensure we can get updates for this
 */

require 'updater/plugin-update-checker.php';

$myUpdateChecker = new Puc_v4p10_Vcs_PluginUpdateChecker(
	new Puc_v4p10_Vcs_GitLabApi('https://gitlab.com/boldorion-public/plugins/theeventcalendar-shortcodes', null, 'plugins'),
	__FILE__,
	'theeventcalendar-shortcodes'
);

$myUpdateChecker->setBranch('master');


/*
 * Let's get into the actual plugin now.
 */
 
 //Styles
add_action('wp_enqueue_scripts', 'align_event_stylings_with_elementor');
function callback_for_setting_up_scripts() {
    wp_register_style( 'boldorion', plugins_url('css/style.css'), __FILE__ );
}
 
function start_event($attr,$content){
    $queried_object = get_queried_object();

    if ( $queried_object ) {
        $post_id = $queried_object->ID;
    }
    
    echo do_shortcode('[tribe_event_inline id="'.$post_id.'"]'.$content.'[/tribe_event_inline]');
}
add_shortcode("event","start_event");


function event_organizer($attr,$content){
    $queried_object = get_queried_object();

    if ( $queried_object ) {
        $post_id = $queried_object->ID;
    }
    
    $return = do_shortcode('[tribe_event_inline id="'.$post_id.'"]{organizer}[/tribe_event_inline]');
	if (strlen($return)>0){
		echo "Hosted by " . $return;
	}
}
add_shortcode("event_organizer","event_organizer");


function event_time($attr,$content){
    $queried_object = get_queried_object();

    if ( $queried_object ) {
        $post_id = $queried_object->ID;
    }
	
	$starttime = do_shortcode('[tribe_event_inline id="'.$post_id.'"]{start_time}[/tribe_event_inline]');
	$endtime = do_shortcode('[tribe_event_inline id="'.$post_id.'"]{end_time}[/tribe_event_inline]');
	
	if (strlen($starttime) > 3){
	$starttime = " at " . $starttime;}

	if (strlen($endtime) > 3){
	$endtime = " at " . $endtime;}

    
    $return = do_shortcode('[tribe_event_inline id="'.$post_id.'"]Starts {start_date}'.$starttime.'<br>Ends {end_date}'.$endtime.'[/tribe_event_inline]');
	
	if (strlen($return)>0){
		echo $return;
	}
}
add_shortcode("event_time","event_time");

function event_cost($attr,$content){
    $queried_object = get_queried_object();

    if ( $queried_object ) {
        $post_id = $queried_object->ID;
    }
    
    $return = do_shortcode('[tribe_event_inline id="'.$post_id.'"]{cost:formatted}[/tribe_event_inline]');
	if (strlen($return)>0){
		echo $return;
	}
}
add_shortcode("event_cost","event_cost");


function event_button($attr,$content){
    $queried_object = get_queried_object();

    if ( $queried_object ) {
        $post_id = $queried_object->ID;
    }
	
	$data 	 = get_post_meta( $post_id, '_EventURL', true );
	if (strlen($content)>0)
	{
		if ((strlen($data)<3))
		echo  "<style>.eventbutton{display:none!important}</style>";
		
		$venue = do_shortcode('[tribe_event_inline id="'.$post_id.'"]{venue}[/tribe_event_inline]');
		if ((strlen($venue)<3))
		echo  "<style>.event-location{display:none!important}</style>";
		return;
	}
	else {
		return $data;
	}
}

add_shortcode("event_button","event_button");